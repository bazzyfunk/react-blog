import React from "react";
import "./App.css";
import Posts from "./component/Posts";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PostPage from "./component/PostPage";
import Homepage from "./component/Homepage";
import Navbar from "./component/Navbar";

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/blog" component={Posts} />
          <Route exact path="/blog/:postId" component={PostPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
