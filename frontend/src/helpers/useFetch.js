import { useState, useEffect } from "react";

export default function useFetch(url) {
  // Create a reusable useFetch function
  const [data, updateData] = useState([]);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(data => updateData(data));
  }, [url]);

  return data;
}
