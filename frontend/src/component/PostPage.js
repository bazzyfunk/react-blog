import React, { useState, useEffect } from "react";

export default props => {
  const { postId } = props.match.params;

  // Set data as a state
  const [dataState, updateData] = useState([]);

  useEffect(() => {
    fetch("http://localhost:3000/posts")
      .then(response => response.json())
      .then(data => updateData(data[postId]));
  }, [postId]);

  return (
    <div className="container">
      <h3>I am post #{postId}</h3>
      <h4>Title : {dataState.name}</h4>
      <span>{dataState.content}</span>
      <div />
    </div>
  );
};
