import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <nav>
      <h1>Mi Bioutifoul Bloggo</h1>
      <NavLink to="/" exact>
        Home
      </NavLink>
      <NavLink to="/blog">Blog</NavLink>
    </nav>
  );
};

export default Navbar;
