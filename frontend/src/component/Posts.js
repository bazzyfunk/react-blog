import React from "react";
import useFetch from "../helpers/useFetch";
import { Link } from "react-router-dom";

export default function Posts() {
  // Récupérer la liste des posts avec la fonction useFetch
  const data = useFetch("http://localhost:3000/posts");

  return (
    <div className="container">
      <h2>THIS MY BLOG PAGE</h2>
      {data.map(el => (
        <div key={el.id}>
          <h3>
            Post #{el.id} : {el.name}
          </h3>
          <span>{el.content}</span>
          <br />
          <Link to={"blog/" + el.id}>See post</Link>
        </div>
      ))}
    </div>
  );
}
